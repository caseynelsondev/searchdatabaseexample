# README #

# **Overview**#

The purpose of this challenge is to provide a relevant, uniform code example by demonstrating use of the Android platform's core frameworks to implement a simple feature common to many applications -- search.

# Goal #

On the initial application startup, if assets/item.csv hasn't been parsed out and stored into a database, run a routine to do so. If the user minimizes or closes the application, this routine should still continue until finished. When finished, if the user is currently viewing the application, a list should be populated with all results (i.e. an empty query should display all items). Once results are present, a menu item should be displayed to allow the user to provide a search query that will filter the list of displayed results by dispatching an additional query. Note: Query strings should filter on the "Item Description" column of the CSV.

# Components/Framework demonstrated #

1. Usage of each of the core Android framework components Activity, Service, BroadcastReceiver, and ContentProvider.
2. Usage of the ToolBar to receive the user's search input that updates the ui as the user types.
3. Knowledge of File parsing, and type conversion.
4. Knowledge of database creation, insertion, and querying.
5. Use of a SharedPreferences object to persist global application state.
6. Useage of a ListView, Adapter and user feedback while no results are present.
7. Use of standard Java naming conventions (Beans Conventions), Android best practices, and code comments where necessary.
8. Usage of an SQL statement to query search results for both partial and exact textual matches. Results queried through a ContentResolver, and the number of results, as well as the results themselves are displayed.