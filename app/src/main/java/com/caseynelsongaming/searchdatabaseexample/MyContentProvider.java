package com.caseynelsongaming.searchdatabaseexample;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by Casey on 9/8/2016. Content Provider class that allows access to the
 * items database.
 */
public class MyContentProvider extends ContentProvider {


    public static final String AUTHORITY = "com.caseynelsongaming.searchdatabaseexample.MyContentProvider";
    public static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY);
    public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, DatabaseHelper.TABLE_NAME);
    public SQLiteDatabase db;
    public DatabaseHelper dbHelper;

    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //Get access to database so we can search
        dbHelper = new DatabaseHelper(getContext());
        db = dbHelper.getWritableDatabase();

        //Setup builder and table to search
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables("item_table");

        //Want all the columns so projection is null
        Cursor cursor = builder.query(db, null, selection, selectionArgs, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
