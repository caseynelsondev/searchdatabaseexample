package com.caseynelsongaming.searchdatabaseexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Casey on 9/6/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    //Database Version
    private static final int DATABASE_VERSION = 1;

    //Database Name
    public static final String DATABASE_NAME = "itemsearch.db";

    //Item table name
    public static final String TABLE_NAME = "item_table";

    //Items table column names
    public static final String COL_1 = "_id";
    public static final String COL_2 = "upc";
    public static final String COL_3 = "item_description";
    public static final String COL_4= "manufacturer";
    public static final String COL_5 = "brand";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    //Creating table
    private static final String  ITEM_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COL_1 + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    COL_2 + " INTEGER" + COMMA_SEP +
                    COL_3 + TEXT_TYPE + COMMA_SEP +
                    COL_4 + TEXT_TYPE + COMMA_SEP +
                    COL_5 + TEXT_TYPE + " )";

    //Deleting table
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    //Indexing item description column for performance
    private static final String SQL_INDEX_ITEM_DESCRIPTION = "CREATE INDEX item_index ON item_table(item_description)";

    //Array of column headers to use for inserting initial data
    private static final List<String> COLUMNS = new ArrayList<String>(){{
        add(COL_1); add(COL_2);add(COL_3); add(COL_4); add(COL_5);
    }};


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //TODO PUT IN BACKGROUND THREAD JUST TESTING

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ITEM_TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);

    }

    public boolean insertData(List<String[]> list){
        SQLiteDatabase db = this.getWritableDatabase();

        List<String[]> insertList = new ArrayList(list);
        long result = 0;

        try {
            //Doing a bulk insert that is only one transaction versus a transaction for each
            //insert without doing the bulk insert
            db.beginTransaction();

            for (String[] stringArray : insertList) {
                Iterator cols = COLUMNS.iterator();
                ContentValues contentValues = new ContentValues();

                for (String data : stringArray) {
                    contentValues.put(cols.next().toString(), data);
                }

                result = db.insert(TABLE_NAME, null, contentValues);

            }
            db.setTransactionSuccessful();
        }catch (SQLiteException e){}
            finally{
                db.endTransaction();

            if(result == -1)
                return false;
            else
                return true;

        }
    }

    public void indexColumn(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_INDEX_ITEM_DESCRIPTION);
    }

    /*//Getting Item Count
    public int getItemsCount(){
        String countQuery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        return count;
    }*/




    public void deleteTable(SQLiteDatabase db){
        db.execSQL(SQL_DELETE_ENTRIES);
    }

    /*public Cursor getWordMatches(String query, String[] columns) {

        //For partial search, surround with %%
        String search = "%" + query + "%";
        String selection = COL_3 + " LIKE ?";
        String[] selectionArgs = new String[]{search};

        return query(selection, selectionArgs, columns);
    }

    private Cursor query(String selection, String[] selectionArgs, String[] columns) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TABLE_NAME);

        Cursor cursor = builder.query(db, null, selection, selectionArgs, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;

    }*/
}
