package com.caseynelsongaming.searchdatabaseexample;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Casey on 9/7/2016.
 * Intent Service class that will populate the database when the app is opened
 * for the first time.
 */
public class LoadDatabaseService extends IntentService {
    SharedPreferences sharedPref;
    DatabaseHelper myDB;
    private static final String TAG = "LoadDataBaseService";

    public LoadDatabaseService() {
        super("LoadDatabaseService");
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {

        Log.d(TAG, "Service Started");

        //Setup Database
        myDB = new DatabaseHelper(this);

        Log.d(TAG, "Database created");

        //Grab csv file to parse
        InputStream inputStream = getResources().openRawResource(R.raw.smalldata);
        CSVReader csv = new CSVReader(inputStream);
        List<String[]> itemList = csv.read();

        //Insert items into database and index item_description column
        Log.d(TAG, "Inserting values into table");
        myDB.insertData(itemList);
        myDB.indexColumn();

        //Use getSharedPreferences to access across multiple classes
        //Database has been created
        sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.database_created), true);
        editor.putBoolean(getString(R.string.load_initial_list), true);
        editor.commit();

        //Setup broadcast and send out that database info is ready!
        Intent localIntent = new Intent("com.caseynelsongaming.searchdatabaseexample.BROADCAST");
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

    }
}
