package com.caseynelsongaming.searchdatabaseexample;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity  {

    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    boolean mIsReceiverRegistered = false;
    private ResponseReceiver mReceiver = null;
    private SharedPreferences sharedPref;
    private DatabaseHelper myDB;
    private ListView mListView;
    private SimpleCursorAdapter mCursorAdapter;
    private TextView mRowCount;
    private RelativeLayout mLoadingDB;
    private boolean mDatabaseCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRowCount = (TextView)findViewById(R.id.rowCount);
        mLoadingDB = (RelativeLayout)findViewById(R.id.loadingDatabase);

        //Setup Toolbar
        mToolbar = getToolbar();

        //Get sharedPref to see if database has already been created. If it has, don't create db
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        mDatabaseCreated = sharedPref.getBoolean(getString(R.string.database_created), false);

        if(!mDatabaseCreated) {
            Toast.makeText(MainActivity.this, "Creating Database", Toast.LENGTH_SHORT).show();
            mLoadingDB.setVisibility(View.VISIBLE);
            //Create new intent to start LoadDataBaseService and fill database
            Intent mServiceIntent = new Intent(this, LoadDatabaseService.class);
            startService(mServiceIntent);

        }
        else{
            mLoadingDB.setVisibility(View.GONE);

        }

        //Show all items if there is an empty query/when database first loads
        if(mDatabaseCreated) {
           loadInitialList();
        }

    }

    @Override
    protected void onStart(){
        super.onStart();

        //Register receiver in onStart so activity is running and
        //manipulation of UI can happen
        if(!mIsReceiverRegistered){
            if(mReceiver == null){
                mReceiver = new ResponseReceiver();
            }
            IntentFilter mStatusIntentFiler = new IntentFilter("com.caseynelsongaming.searchdatabaseexample.BROADCAST");
            LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,mStatusIntentFiler);
            mIsReceiverRegistered = true;
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        //Get sharedPref to see if database has already been created. If it has, don't create db
        //sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        boolean mLoadInitialList = sharedPref.getBoolean(getString(R.string.load_initial_list), false);

        //Makes sure list populates if you leave app while database loads and come back
        if(mLoadInitialList){

            mLoadingDB.setVisibility(View.GONE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(getString(R.string.load_initial_list), false);
            editor.commit();

            loadInitialList();
        }
    }

    @Override
    protected void onStop(){
        super.onStop();

        //Unregister receiver when activity is not in foreground/visible
        if (mIsReceiverRegistered){
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
    }

    //single top mode so handle new intents here!
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        //Keyboard button "OK" has been pressed.
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            //For partial search, surround with %%
            String search = "%" + query + "%";
            String selection = DatabaseHelper.COL_3 + " LIKE ?";
            String[] selectionArgs = new String[]{search};
            Cursor cursor = this.getContentResolver().query(MyContentProvider.CONTENT_URI, null, selection, selectionArgs, null, null);

            //Mapping which columns of data from the query go to which views
            String[] bindFrom = { DatabaseHelper.COL_1, DatabaseHelper.COL_2, DatabaseHelper.COL_3, DatabaseHelper.COL_4, DatabaseHelper.COL_5};
            int[] bindTo = {R.id.itemID, R.id.itemUPC, R.id.itemDescription, R.id.itemManufacturer, R.id.itemBrand};

            //Setup listview and adapter
            mListView = (ListView)findViewById(R.id.listview);
            mCursorAdapter = new SimpleCursorAdapter(this, R.layout.item_row, cursor, bindFrom, bindTo, 0);

            if(cursor != null) {
                mRowCount.setText("" + cursor.getCount());
            }
            else{
                mRowCount.setText(R.string.no_records);
            }

            mListView.setAdapter(mCursorAdapter);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        //Set search menu item/hint up
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView)menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Nullable
    private Toolbar getToolbar() {

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        if( myToolbar != null) {
            setSupportActionBar(myToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }
        return myToolbar;
    }


    // Broadcast receiver for receiving status updates from the IntentService
    private class ResponseReceiver extends BroadcastReceiver {

        // Prevents instantiation
        private ResponseReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "Database Ready! Search Away!", Toast.LENGTH_SHORT).show();
            mLoadingDB.setVisibility(View.GONE);
            loadInitialList();

        }
    }

    //Creates a query and loads the listview with all the rows of the database
    private void loadInitialList() {

        //display entire list when app first comes up
        Cursor cursor = this.getContentResolver().query(MyContentProvider.CONTENT_URI, null, null, null, null, null);

        //Maps views in the layout to their corresponding data found in the cursor/database column
        String[] bindFrom = {DatabaseHelper.COL_1, DatabaseHelper.COL_2, DatabaseHelper.COL_3, DatabaseHelper.COL_4, DatabaseHelper.COL_5};
        int[] bindTo = {R.id.itemID, R.id.itemUPC, R.id.itemDescription, R.id.itemManufacturer, R.id.itemBrand};

        //Setup listview and adapter
        mListView = (ListView) findViewById(R.id.listview);
        mCursorAdapter = new SimpleCursorAdapter(this, R.layout.item_row, cursor, bindFrom, bindTo, 0);

        if (cursor != null) {
            mRowCount.setText("" + cursor.getCount());
        } else {
            mRowCount.setText(R.string.no_records);
        }

        mListView.setAdapter(mCursorAdapter);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.load_initial_list), false);
        editor.commit();
    }
}
