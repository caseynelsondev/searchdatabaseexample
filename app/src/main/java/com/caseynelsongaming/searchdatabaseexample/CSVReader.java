package com.caseynelsongaming.searchdatabaseexample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Casey on 9/7/2016.
 * Class that parses a Comma Separated Value text into an
 * ArrayList of String arrays. Each item in the Arraylist is one row of the
 *text file
 */
public class CSVReader {
    InputStream inputStream;

    public CSVReader(InputStream is){
        this.inputStream = is;
    }

    public List<String[]> read() {
        //Result List is an ArrayList of String Arrays each being a row in the db
        List<String[]> resultList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String csvLine;
            while((csvLine = reader.readLine()) != null){
                String[] row = csvLine.split(",");
                resultList.add(row);
            }
            }catch(IOException e){
                throw new RuntimeException("Error reading the CSV file:" + e);
        } finally{
            try{
                inputStream.close();
            }catch (IOException e){
                throw new RuntimeException("Error while closing input stream" + e);
            }
        }

        return resultList;
    }
}
